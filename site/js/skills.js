'use strict'

function isElementInViewport (element){
    if (typeof jQuery === "function" && element instanceof JQuery) {
        element = element[0];
    }
    var rect = element.getBoundingClientRect();
    return(
        (rect.top <= 0 && rect.bottom >=0)
        ||
        (rect.bottom >= (window.innerHeight || document.documentElement.clientHeight) &&
            rect.top <= (window.innerHeight || document.documentElement.clientHeight))
        ||
        (rect.top >= 0 &&
            rect.bottom <= (window.innerHeight || document.documentElement.clientHeight))
    );
}


var scroll = window.requestAnimationFrame ||
             function(callback){
                 window.setTimeout(callback,
                    1000/60)
             };

var elementToShow = document.querySelectorAll('.progress-on-scroll');
function loop() {
    elementToShow.forEach(function (element){
        if (isElementInViewport(element)){
            element.classList.add('is-visible');
        }else {
            element.classList.remove('is-visible');
        }
    });
    scroll(loop);
}

loop();
