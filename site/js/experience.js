'use strict'
/**
 * 
 * @param {*} cardHeaders 
 * @param {*} cardContents 
 */
const pageLoad =(cardHeaders,cardContents) =>{
    for(var i = 0; i< cardHeaders.length; i++ ){
        cardHeaders[i].classList.add("Header-" + i)
        cardContents[i].classList.add("Contend-" + i)

        if (i != 0){
            cardHeaders[i].style.cursor="pointer"
            cardContents[i].classList.add("d-none")
        }
        else{
            cardHeaders[i].style.cursor="default"
        }
    }
}

/**
 * 
 * @param {*} cardElement 
 */
const getIdOfCard = (cardElement) => {
    return cardElement.classList.item(2).replace(/[^0-9]/g,'')
}

/**
 * @param {*} cardHeaders 
 */
const getLastExperience = (cardHeaders) =>{
    for (const cardHeader of cardHeaders){
        if (cardHeader.style.cursor == "default"){
            return cardHeader
        }
    }
}

/**
 * @param {*} headerId
 * @param {*} cardContents
 * 
 */
const getCardContentForId=(headerId,cardContents)=>{
    for (const cardContent of cardContents){
        if (headerId === getIdOfCard(cardContent)){
            return cardContent
        }
    }
}

document.addEventListener("DOMContentLoaded",() =>{

    const ExperienceCardHeaders = document.getElementsByClassName("experience-card-header")
    const ExperienceCardContents = document.getElementsByClassName("content-description")

    pageLoad(ExperienceCardHeaders, ExperienceCardContents)

    for (const cardHeader of ExperienceCardHeaders){
        cardHeader.addEventListener('click', (event) =>{
            //searching the last showed and hr-orangered
            // --- Get the header id to deactvate the content
            var lastCardHeader = getLastExperience(ExperienceCardHeaders)
            if (cardHeader.style.cursor == "pointer"){
                /* 
                    change color of hr-class bei removing hr-class orangered
                     to the last card and add it to the new clicked
                 */
                lastCardHeader.lastElementChild.classList.remove("bg-oranged")
                cardHeader.lastElementChild.classList.add("bg-oranged")
                // change cursor
                for(const elt of ExperienceCardHeaders){
                    if (elt === cardHeader){
                        //elt.style.cursor = "default"
                        cardHeader.style.cursor= "default"
    
                    }
                    else{
                        elt.style.cursor="pointer"
                    }
                }
                /*
                    change desable the last content and show the contend corresponding
                    to the selected header-card
                 */
                // desable last content
                const lastContent = getCardContentForId(getIdOfCard(lastCardHeader),ExperienceCardContents)
                lastContent.classList.add("d-none")
                // enable content of selected header
                const newContent = getCardContentForId(getIdOfCard(cardHeader),ExperienceCardContents)
                newContent.classList.remove("d-none") 
            }
        })
    }
});