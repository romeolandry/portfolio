'use strict';

const Alt = (Tag,Monate, Jahr) =>{
    var heute = new Date();
    var Geburtsdatum = new Date(Jahr, Monate, Tag);
    var Alter = heute - Geburtsdatum;

    var Alter = Math.floor(Alter/31536000000);
    return Alter
}

function padTo2Digits(num) {
    return num.toString().padStart(2, '0');
  }

const formatDate = (date) => {
    return [
      padTo2Digits(date.getMonth() + 1),
      padTo2Digits(date.getDate()),
      date.getFullYear(),
    ].join('.');
  }

document.addEventListener("DOMContentLoaded", () =>{
    // set Alter
    document.getElementById("alt").innerHTML = Alt(13,8,1991)
    // select all card header class
    const cardHeaders = document.getElementsByClassName("card-header")

    for (const cardHeader of cardHeaders){
        const cardBody = cardHeader.nextElementSibling
        cardHeader.addEventListener('click',(event)=>{
            // get ElementClass content d-done
            cardBody.classList.toggle("d-none")
        })
    }

    // set date fo the day 

    document.getElementById("today").innerHTML = formatDate(new Date());

    // set  year for footer
    document.getElementById("year").innerHTML = new Date().getFullYear();
});